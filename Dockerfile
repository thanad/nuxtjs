FROM node:8.11-slim

RUN apt-get update \
 && apt-get install -y git

WORKDIR /usr/src/app

RUN npm install -g @vue/cli @vue/cli-init

COPY . /usr/src/app/

RUN npm install

# Build app
# RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000

# start command
ENTRYPOINT [ "npm", "run", "dev" ]

