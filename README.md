# nuxt

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
docker-compose run --rm --entrypoint='npm install' nuxt

# serve with hot reload at localhost:3000
docker-compose up -d

# show server logs
docker-compose logs -f nuxt
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

