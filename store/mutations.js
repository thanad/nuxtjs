const mutations = {
  findingX(stage, n ){
    stage.xValue = ( n * n ) - n + 3;
    return stage.xValue;
  },
  findingY(stage){
    stage.yValue = (99 - (10 * 2)) - 24;
  },
  findingX2(stage, n ){

    let x = "";
    for (let a = n; a > 0 ; a--){
      if ( a == 1){
        x += "5";
      } else {
        x += a;
      }
    }
    stage.x2Value = x
  },
}

export default mutations
